<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsfeedRequest;
use App\Newsfeed;
use Illuminate\Http\Request;

class NieuwsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages/nieuws');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param NewsfeedRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(NewsfeedRequest $request)
    {
        $bli = new Newsfeed();
        $bli->title = $request->title;
        $bli->preface = $request->preface;
        $bli->article = $request->article;
        $bli->filename = $request->filename;

        $bli->save();

        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
