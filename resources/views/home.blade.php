@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/lwr.png') }}">
    <div class="container margins-con spaceing">
        <h2 class="">Dashbaord</h2>
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card select-card">
                    <div class="modal-header">
                        <h3>Nieuwsbericht toevoegen</h3>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('nieuws.create') }}">
                        @csrf
                        <input class="form-control" type="text" name="title" id=""placeholder="Titel">
                        <textarea class="form-control" id="" name="preface" cols="15" rows="3" placeholder="Inleiding"></textarea>
                        <textarea class="form-control" name="article" id="" cols="30" rows="10" placeholder="Nieuws bericht" maxlength="250"></textarea>
                        <button class="" type="submit" name="image" value="Upload image">Upload hier je afbeelding</button>
                        <button type="submit" name="Submit" value="Add new article" class="btn prim-btn">Opslaan </button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
