<nav class="navbar navbar-expand bg-white shadow-sm" >
    <hr class="nav-line">
<div class="container">
        <div class="collapse navbar-collapse d-flex align-content-center" id="navbarNavDropdown">
            <a class="" href="{{ url('') }}" style="width: 4%; margin-right: 20px">
                <img src="{{ asset('storage/img/dwvlogo.png') }}" style="width: 100%">
            </a>
        <ul class="navbar-nav " style="width: 100%">
            <li class="nav-item active nav-elementen">
                <a class=" nav-link" href="{{ url('') }}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown nav-elementen">
                <a class= " nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" >Algemeen</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="{{ url('about') }}">Over DWV</a>
                    <a class="dropdown-item" href="{{ url('bestuur') }}">Bestuur</a>
                    <a class="dropdown-item" href="{{ url('faq') }}">Veel gestelde vragen </a>
                    <a class="dropdown-item" href="{{ url('links') }}">Links / Documenten </a>
                </div>
            </li>
            <li class="nav-item dropdown nav-elementen dropdown-submenu">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-haspopup="true" >Afdelingen</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a tabindex="-1" class="dropdown-item" href="{{ url('waterpolo') }}">Waterpolo</a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="dropdown-item" href="#">Wedstrijdzwemmen</a>
                            <a class="dropdown-item" href="#">Recreatief zwemmen</a>
                        </li>
                    </ul>
                    <a class="dropdown-item" href="{{ url('zwemmen') }}">Wedstrijdzwemmen</a>
                    <a class="dropdown-item" href="{{ url('recreatief') }}">Recreatief zwemmen</a>
                </div>
            </li>
            <li class="nav-item nav-elementen">
                <a class="nav-link" href="{{ url('nieuws') }}">Nieuws</a>
            </li>
            <li class="nav-item nav-elementen">
                <a class="nav-link" href="{{ url('kalender') }}">Kalender</a>
            </li>
            <li class="nav-item nav-elementen">
                <a class="nav-link" href="{{ url('contact') }}">Contact</a>
            </li>

            <div class="col d-flex justify-content-end">
                <button type="button" class="prim-btn font-weight-bold" data-toggle="modal" data-target="#GratisTrainenModal">
                    4x gratis meetrainen
                </button>
            </div>
            {{--Edit modal--}}
            <div class="modal" id="GratisTrainenModal" tabindex="-1" role="dialog"
                 aria-labelledby="AddPlayersModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class=" nav-elementen modal-title" >4x gratis meetrainen</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span  aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class=" modal-body">
                            <p class="text-dark">The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, </p>
                            <br>
                            <h6>Persoonlijke gegevens</h6>
                            <form method="post" action="">

                                @csrf
                                <input class="form-control" type="text" name="fname" placeholder="Voornaam"
                                       required>
                                <br>
                                <input class="form-control" type="text" name="lanme" placeholder="Achternaam"
                                       required>
                                <br>
                                <input class="form-control" type="email" name="email" placeholder="E-mail adres"
                                       required>
                                <br>
                                <input class="form-control" type="number" name="phone" placeholder="Telefoon"
                                           required>
                                <br>
                                <div class="row">
                                    <div class="col-6">
                                        <input class="form-control" type="date" name="age" placeholder="Geboorte dattum"
                                               required>
                                    </div>
                                    <div class="col-6">

                                        <select class="form-control">
                                            <option value="waterpolo">Waterpolo</option>=
                                            <option value="wedstrijdzwemmen">Wedstrijdzwemmen</option>
                                            <option value="zelfReceatief">Zelfstandig recreatiefzwemmen</option>
                                            <option value="masterzwemmen">Masterzwemmen</option>
                                            required
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <input class="form-check-input ml-2" type="checkbox" name="remember"
                                       id="remember" required>
                                <label class="form-check-label ml-4" for="remember">Ik ga akkoord met de <a href="#">algemene voorwaarden</a> van DWV</label>
                                <br>
                                <div class=" mt-3 ">
                                    <button type="submit" name="Submit" value="Add new article"
                                            class="modal-btn second-btn">Verzenden
                                    </button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </ul>
    </div>

</div>
    <hr class="nav-line">
</nav>
