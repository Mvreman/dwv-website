@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/lwr.png') }}">
    <div class="container margins-con spaceing">
        <div class="card select-card">
            <div class="card-body">
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active row">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                </div>
                                <div class="carousel-item row">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                </div>
                                <div class="carousel-item row">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                    <img class="col-4 imgCarousel" src="{{ asset('storage/img/13173210_1194589830565155_9087567648200996153_o.png') }}">
                                </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        <div class="mb-3">
                            <h3 class=" mt-3"> Hoe het allemaal is begonnen</h3>
                            <p> Op 20 juli 1931 zag de Doesburgsche Watersport Vereniging, hierna DWV genoemd, voor het eerst het levenslicht. Op de kenmerkende golven van het ons zo geliefde water ontwikkelde DWV zich in de voorbijgaande jaren. In rustig water ontspannend en genietend van fraai behaalde successen, dan weer in woeste golven waarin het door felle tegenwind dreigde kopje onder te gaan. Door sluiting in de jaren tachtig van het openlucht Dr. Brandbad in Doesburg en het ontbreken van een eigen overdekte accommodatie, werd DWV gedwongen haar badwater buiten de grenzen van Doesburg te zoeken met negatieve gevolgen voor het ledental. Maar met kracht van een ieder die DWV een warm hart toedraagt, wist zij te overleven. In deze periode is veel dank verschuldigd aan al die leden die hun steentje hierin hebben bijgedragen om het voortbestaan te waarborgen. Deze donkerste periode voor de vereniging werd definitief afgesloten met de komst van het zwembad Den Helder in Doesburg. Mede door de terugkeer van een eigen zwemafdeling groeide het aantal leden van een kleine 50 destijds na het sluiten van het openluchtbad gestaag naar een 237 rond 2011 en thans met een huidig ledenaantal van 185. Vandaag de dag is DWV verankerd in de samenleving van Doesburg en omgeving en draagt zij bij in de maatschappelijke verantwoordelijkheid van deze gemeente. In 2021 zal de vereniging haar 90-jarig jubileum vieren!</p>
                        </div>
                    </div>
            </div>
        </div>






@endsection
