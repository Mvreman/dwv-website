@extends('layouts.app')

@section('content')
        <img class="banner" src="{{ asset('storage/img/lwr.png') }}">
    <div class="container margins-con spaceing">

        <h2 class="">Bestuur DWV</h2>
             <div class="row mt-4 mb-4">
                 <div class="col-3">
                     <div class="card select-card">
                         <div class="card-body">
                             <div class="d-flex">
                                 <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                             </div>
                             <h4 class="text-center font-weight-bold">Voorzitter</h4>
                             <p class="text-center">Walther Eenstroom</p>
                             <a href="mailto:voorzitter@dwvdoesburg.nl?"><button class="prim-btn center-btn" >Contact</button></a>
                         </div>
                     </div>
                 </div>
                 <div class="col-3">
                     <div class="card select-card">
                         <div class="card-body">
                             <div class="d-flex">
                                 <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                             </div>
                             <h4 class="text-center font-weight-bold">Secretaris</h4>
                             <p class="text-center">Henkjan Kets</p>
                             <a href="mailto:secretaris@dwvdoesburg.nl?"><button class="prim-btn center-btn" >Contact</button></a>
                         </div>
                     </div>
                 </div>
                 <div class="col-3">
                     <div class="card select-card">
                         <div class="card-body">
                             <div class="d-flex">
                                 <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                             </div>
                             <h4 class="text-center font-weight-bold">Penningmeester</h4>
                             <p class="text-center">Rick Kleine</p>
                             <a class="mailto:penningmeester@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                         </div>
                     </div>
                 </div>
            </div>
    <h2 class="">Commissies</h2>
    <div class="row mt-4 mb-4">
        <div class="col-3">
            <div class="card select-card">
                <div class="card-body">
                    <div class="d-flex">
                        <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                    </div>
                    <h4 class="text-center font-weight-bold">Waterpolo</h4>
                    <p class="text-center">Nicole Rutten</p>
                    <a href="mailto:voorzitterpolo@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card select-card">
                <div class="card-body">
                    <div class="d-flex">
                        <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                    </div>
                    <h4 class="text-center font-weight-bold">Zwemmen</h4>
                    <p class="text-center">Tessa aan de Meullen</p>
                    <a href="mailto:voorzitterzwem@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card select-card">
                <div class="card-body">
                    <div class="d-flex">
                         <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                    </div>
                    <h4 class="text-center font-weight-bold">Seniorencoördinator</h4>
                    <p class="text-center">Roy Campschroer</p>
                    <a href="mailto:seniorcoordinatorpolo@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="card select-card">
                <div class="card-body">
                    <div class="d-flex">
                        <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                    </div>
                    <h4 class="text-center font-weight-bold">Jeugdcoördinator</h4>
                    <p class="text-center">Alex Vrendebarg</p>
                    <a href="mailto:jeugdcoordinatorpolo@dwvdoesburg.nl"> <button class="prim-btn center-btn" >Contact</button></a>
                </div>
            </div>
        </div>
    </div>
        <h2 class="">Overig</h2>
        <div class="row spaceing mt-4 ">
            <div class="col-3">
                <div class="card select-card">
                    <div class="card-body">
                        <div class="d-flex">
                            <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                        </div>
                        <h4 class="text-center font-weight-bold">Sponsorcommissie</h4>
                        <p class="text-center">Carolien Kleine</p>
                        <a href="mailto:sponsorcommissie@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card select-card">
                    <div class="card-body">
                        <div class="d-flex">
                            <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                        </div>
                            <h4 class="text-center font-weight-bold">Ledenadministratie</h4>
                            <p class="text-center">Olga Staring</p>
                            <a href="mailto:ledenadministratie@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card select-card">
                    <div class="card-body">
                        <div class="d-flex">
                            <img class="board-img mb-3" src="{{ asset('storage/img/users.png') }}" >
                        </div>
                        <h4 class="text-center font-weight-bold " style="font-size: 20px">Activiteitencommissie</h4>
                        <p class="text-center">Jessica Orth</p>
                        <a href="mailto:voorzitteractiviteitencommissie@dwvdoesburg.nl"><button class="prim-btn center-btn" >Contact</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
