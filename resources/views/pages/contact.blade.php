@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/waterpolo.png') }}">
    <div class="container margins-con">
        <div class="row">
            <div class="col-7 contact">
                <h3>Stuur ons een mail met uw vragen </h3>
                <form action="">
                    <label for="fname">Uw gegevens</label>
                    <input class="form-control" type="text" id="fname" name="fname" placeholder="Voornaam" required>
                    <input class="form-control" type="text" id="lname" name="lname" placeholder="Achternaam" required>
                    <input class="form-control" type="text" name="email" placeholder="Email adres" required>
                    <input class="form-control" type="text" name="subject" placeholder="Onderwerp" required>
                    <textarea class="form-control" name="" id="" cols="30" rows="12" placeholder="Uw bericht" required></textarea >
                    <button type="submit" class="second-btn">Verzend</button>
                </form>
            </div>
            <div class="col-5 sidebar d">
                <h3 class="text-white">Algemene gegevens </h3>
                <p>Doesburgse Watersport Vereniging</p>
                <p>Enghuizen 2,  6983 HL  Doesburg</p> <br>
                <p>Mail:       info@dwvdoesburg.nl</p>
                <p>Kvk nr.    40120783</p><br>
                {{--bestuurs gegevens--}}
                <h4 class="font-weight-bold"style="font-size: 16px">Contact gegevens bestuursleden</h4>
                <div class="members">
                    <p class="font-weight-bold">Voorzitter</p>
                    <p>Walther Eenstroom</p>
                    <p>voorzitter@dwvdoesburg.nl</p>
                </div>
                <div class="members">
                    <p class="font-weight-bold">Secretaris</p>
                    <p>Henkjan Kets</p>
                    <p>secretaris@dwvdoesburg.nl</p>
                </div>
                <div class="members">
                    <p class="font-weight-bold">Penningmeester</p>
                    <p>Rick Kleine</p>
                    <p>penningmeester@dwvdoesburg.nl</p>
                </div>
                <div class="members">
                    <p class="font-weight-bold">Polocommissie</p>
                    <p>polocommissie@dwvdoesburg.nl</p>
                </div>
                <div class="members">
                    <p class="font-weight-bold">Zwemcommissie</p>
                    <p>zwemcommissie@dwvdoesburg.nl</p>
                </div>
            </div>
        </div>
    </div>
    <div style="width: 100%">
        <iframe width="100%" height="400" src="https://maps.google.com/maps?width=100%&height=600&hl=nl&q=%20Den%20Helder%201%2C%206982%20DT%20Doesburg+(Zwembad%20Den%20Helder)&ie=UTF8&t=&z=15&iwloc=B&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.mapsdirections.info/nl/maak-een-google-map/">Maak een Google Map</a> van <a href="https://www.mapsdirections.info/nl/">Nederland Kaart</a></iframe>
    </div>
@endsection
