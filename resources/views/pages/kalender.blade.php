@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/overview_zwemmen.png') }}">
    <div class="container margins-con">
        <div class="header-department">
            <h2 class="wp-color">kalender</h2>
            <div class="row players-inner">
                <table id="table-pagination datatable" class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Datum</th>
                        <th scope="col">Title</th>
                        <th scope="col">Edit</th>
                        <th scope="col">Verwijderen</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">test</td>
                            <td style="width: 10%">test</td>
                            <td>test</td>
                            <td><img style="width: 20%" data-toggle="modal" data-target="#EditNewsModal" data-id="" src="{{ asset('storage/icons/ic_edit.png')}}"></td>
                            <div class="modal" id="EditNewsModal" tabindex="-1" role="dialog">
                                <div aria-labelledby="EditNewsModalsModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="AddNewsArticle">Nieuwsbericht wijzigen</h5>
                                                <button type="button" class="close" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form method="post" action="/nieuwsfeed" id="editform">

                                                    <input class="form-control" type="text" name="title" id="title"
                                                           value=""
                                                           required>
                                                    <br>
                                                    <textarea class="form-control" type="text" name="article" id="article"
                                                              required rows="15"></textarea>
                                                    <br>
                                                    <button class="" type="submit" name="image" value="Upload image" id="image">
                                                        Upload hier je afbeelding
                                                    </button>

                                                    <div class="modal-footer">
                                                        <button type="button" class="btn scnd-btn" data-dismiss="modal">
                                                            Exit
                                                        </button>
                                                        <button type="submit" name="Submit" value="Add new article"
                                                                class="btn prim-btn">Wijzig
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <td>
                                <form action="" method="post">
                                    <button class="scnd-btn"><input class="float-right" type="submit" value="Delete"
                                                                    onclick="return confirm ('Are you sure you want to delete artikel, ?')">
                                    </button>
                                </form>
                            </td>
                        </tr>
                =
                    </tbody>
                </table>
            </div>


        </div>
    </div>
@endsection
