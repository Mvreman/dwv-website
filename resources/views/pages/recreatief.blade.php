@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/recreatief.png') }}">
    <div class="container margins-con">
        <div class="header-department">
            <h2 class="wp-color">Recreatiefzwemmen</h2>
            <p>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop</p>
        </div>
        <div class="row zelfstandig-rec">
            <div class="col-6">
                <h3 class="wp-color">Zelfstandig recreatiefzwemmen</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who</p>
            </div>
            <div class="col-6">
                <img  class="rec-img" src="{{ asset('storage/img/recreatiefzwem.png') }}" alt="">
            </div>
        </div>
        <div class="row space-bottom">
            <div class="col-6">
                <h3 class="wp-color">Masterzwemmen</h3>
                <p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who</p>
            </div>
            <div class="col-6">
                <img  class="rec-img" src="{{ asset('storage/img/Recreanten.png') }}" alt="">
            </div>
        </div>
    </div>

@endsection
