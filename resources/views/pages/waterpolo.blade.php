@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/waterpolo.png') }}">
    <div class="container margins-con">
        <h2 class="wp-color">Waterpolo</h2>
        <p> Waterpolo is de “watervariant” van het handbal- of rugbyspel en ontstond halverwege de 19e eeuw. Waterpolo werd in 1885 officieel erkend en is een uitdagende balsport die je in teamverband zwemmend uitoefent. De bedoeling is om een bal zo vaak mogelijk in het doel van de tegenpartij te werpen. Elke geslaagde poging levert één doelpunt op. Het team dat aan het eind van de wedstrijd de meeste doelpunten heeft gemaakt, wint de wedstrijd. Veldspelers mogen de bal maar met één hand tegelijk aanraken. De keepers mogen binnen de 5 meterzone de bal met twee handen aanraken. De bal met twee handen aanraken of de bal onder water duwen als je wordt aangevallen levert een vrije worp op voor de tegenpartij. Samen met jouw team speel je wedstrijden in een competitie. Gemiddeld nemen acht tot elf teams hieraan deel.</p>
        <div class="row mt-4 mb-4">
            <div class="col-6 justify-content-center">
                <h3 class="wp-color">Teams</h3>
                <p> Een team bestaat minimaal uit zes veldspelers en een keeper en maximaal uit 13 spelers bestaan. De spelers dragen een cap, wit voor het thuisspelend team en blauw voor het uitspelend team. Beide keepers hebben een rode cap. Alle spelers hebben oorbeschermers aan hun cap, de keepers in de kleur van het team.</p>
                <br>
                <p> DWV heeft verschillende mogelijkheden om waterpolo te spelen. Zo zijn er een damesteam, heren teams en jeugdteams in verschillende leeftijdscategorieën. Wil je als waterpoloër(ster) alleen maar trainen en geen competitie spelen, ook dan ben je van harte welkom.</p>
                <br>
                <p>Wil je meer weten over een team van DWV? of wil je een team sponsoren</p>
                <div class="mt-2 ">
                    <button type="button" class="prim-btn" data-toggle="modal" data-target="#GratisTrainenModal">
                        Bekijk alle teams
                    </button>
                </div>
            </div>
            <div class="col-6 ">
                <div class="card" style="">
                    <div class="card-body">
                        <!--Carousel Wrapper-->
                        <div  id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
                            <!--Indicators-->
                            <ol class="carousel-indicators ">
                                <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-2" data-slide-to="1"></li>
                                <li data-target="#carousel-example-2" data-slide-to="2"></li>
                                <li data-target="#carousel-example-2" data-slide-to="3"></li>
                            </ol>
                            <!--/.Indicators-->
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item active">
                                    <div class="view">
                                        <img class="d-block w-100" src="{{ asset('storage/teamfotos/Heren 1.jpg') }}"
                                             alt="Heren 1">
                                        <div class="mask rgba-black-light"></div>
                                    </div>
                                    <div class="carousel-caption">
                                        <h3 class="text-white h3-responsive">Heren 1</h3>
                                        <p>2019 / 2020</p>
                                    </div>
                                </div>
                                <div class="carousel-item ">
                                    <div class="view">
                                        <img class="d-block w-100" src="{{ asset('storage/teamfotos/Heren 2.jpg') }}"
                                             alt="Heren 1">
                                        <div class="mask rgba-black-light"></div>
                                    </div>
                                    <div class="carousel-caption">
                                        <h3 class="text-white h3-responsive">Heren 2</h3>
                                        <p>2019 / 2020</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="view">
                                        <img class="d-block w-100" src="{{ asset('storage/teamfotos/Heren 3.jpg') }}"
                                             alt="Heren 1">
                                        <div class="mask rgba-black-light"></div>
                                    </div>
                                    <div class="carousel-caption">
                                        <h3 class="text-white h3-responsive">Heren 3</h3>
                                        <p>2019 / 2020</p>
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="view">
                                        <img class="d-block w-100" src="{{ asset('storage/teamfotos/Dames.jpg') }}"
                                             alt="Heren 1">
                                        <div class="mask rgba-black-light"></div>
                                    </div>
                                    <div class="carousel-caption">
                                        <h3 class="text-white h3-responsive">Dames 1</h3>
                                        <p>2019 / 2020</p>
                                    </div>
                                </div>
                            </div>
                            <!--/.Slides-->
                            <!--Controls-->
                            <a class="carousel-control-prev" href="#carousel-example-2" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-example-2" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                            <!--/.Controls-->
                        </div>
                        <!--/.Carousel Wrapper-->
                    </div>
                </div>
            </div>
        </div>

            <h3 class="wp-color">Wedstrijd</h3>
            <p>Een wedstrijd is verdeeld in vier periodes die, afhankelijk van de leeftijd van de spelers, het competitieniveau en het land, 4 tot 8 zuivere speelminuten duren. Dit betekent dat de tijdmeting wordt stilgelegd tussen het begaan van een overtreding (op het fluitsignaal van één van de één of twee scheidsrechters) en het nemen van de daaropvolgende vrije worp.</p>
        <div class="row mt-4 mb-4">
            <div class="col-6">
                <h4 class="wp-color">Speelveld</h4>
                <p>DDe grootte van het speelveld is afhankelijk van het zwembad. DWV speelt zijn thuiswedstrijden in Zwembad Den Helder in Doesburg, het wedstrijdbad heeft daar een lengte van 25m en een breedte van 12,5 meter.</p>
                <div class="bal">
                <h4 class="wp-color">Waterpolobal</h4>
                    <div class="row balls">
                    <div class="col-3">
                        <img src="{{ asset('storage/img/bal1.png') }}">
                    </div>
                    <div class="col-3">
                        <img src="{{ asset('storage/img/bal2.png') }}">
                    </div>
                    <div class="col-3">
                        <img src="{{ asset('storage/img/bal3.png') }}">
                    </div>
                    </div>
                    <p>Waterpolo speel je met een speciale bal. De herenteams gebruiken een bal die ongeveer even groot is als een voetbal. Deze weegt ongeveer 400 á 450 gram. De dames en de jeugd, tot en met 17 jaar, spelen met een kleinere bal. Een belangrijk kenmerk van de bal is dat hij veel grip heeft, zodat je hem ondanks zijn grootte toch met één hand kunt vasthouden. Als de bal vaak gebruikt wordt verliest hij zijn grip en moet hij vervangen worden.</p>
                </div>
            </div>
            <div class="col-6 pl-3">
                <img class="speelveld" src="{{ asset('storage/img/speelveld.png') }}">
            </div>
        </div>
    </div>
  <div class="bg-img">
    <div class="container">
        <div class="row">
            <div class="col-6 card select-card ">
                <div class="card-body ">
                    <h4 class="wp-color">Techniek</h4>
                    <p>Bij waterpolo leer je een speciale manier van “watertrappelen”, een bal in het water vangen met één hand, gooien en stoten en hoe je een doelpunt moet maken. Ook leer goed borstcrawl zwemmen en dit doe je met het hoofd “boven water”. Dit leer je door veel te oefenen. Wil je waterpolo op een behoorlijk niveau kunnen spelen, dan is regelmatig trainen een “must”. Alle groepen kennen 2 trainingsmomenten per week, van 60 minuten per training, de “jongste jeugd “ (Pupillen/Jeugd <13 jaar) traint 45 minuten per training.</p>
                        <br><h5>Voor wie?</h5> <p> Waterpolo is een sport voor alle leeftijden, van jeugd tot volwassenen. Om mee te kunnen doen moet je wel goed kunnen zwemmen. Speciale diploma's heb je echter niet nodig.</p>
                    <br>
                    <h4 class="wp-color">Kom een keer meetrainen</h4>
                    <p>TLijkt waterpolo je een leuke sport? Kom eens kijken op een training. Wil al meedoen, vul dan op de website het “4x gratis meetrainen” formulier in. Je bent van harte welkom. Heb je vragen? Stuur deze naar <a href="mailto:voorzitterpolo@dwvdoesburg.nl">voorzitterpolo@dwvdoesburg.nl</a> </p>
                    <div class="mt-3 ">
                        <button type="button" class="prim-btn" data-toggle="modal" data-target="#GratisTrainenModal">
                            4x gratis meetrainen
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>


@endsection
