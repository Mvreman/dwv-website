@extends('layouts.app')

@section('content')
    <img class="banner" src="{{ asset('storage/img/wedstrijdzwemmen.png') }}">
    <div class="container margins-con">
        <div class="row header-department">
            <div class="col-8">
            <h2 class="wp-color">Wedstrijdzwemmen</h2>
                <p> Wil je graag in het water zijn, aan je conditie werken en lijkt het je leuk om het in een wedstrijd op te nemen tegen leeftijdsgenoten, dan is wedstrijdzwemmen echt iets voor jou!
                    Met wedstrijdzwemmen kan je al op een jonge leeftijd, vanaf 6 jaar, beginnen. Ben je in het bezit van een A- en B- zwemdiploma en wil je graag jouw zwemslagen verbeteren om nog sneller te kunnen zwemmen, kom dan gezellig bij ons trainen en sluit je aan bij de zwemafdeling.<br>
                    <br>
                    Zwemmen is een sport waarbij het gehele lichaam wordt gebruikt, zowel lichamelijk als mentaal. Binnen het zwemmen ligt niet alleen de nadruk op het sneller zwemmen, maar leren de zwemmers ook omgaan met spanningen, winst en verlies, samenwerken met clubgenoten, het leren beheersen van het lichaam en het kennen en mogelijk verleggen van jouw grenzen.
                </p>
            </div>
            <div class="col-4">
s
            </div>
        </div>
        <div class="row">
            <div class="mb-5 traning col-6">
                <h3>Training</h3>
                <p>Binnen de zwemafdeling van DWV wordt in verschillende doelgroepen getraind. Als je bij ons komt zwemmen, start je bij de opleiding en train je twee keer in de week. In deze doelgroep wordt voornamelijk aan de techniek van de borstcrawl, rugcrawl en schoolslag gewerkt. Ook leren de zwemmers de keerpunten, starten van een startblok en de vlinderslag.
                    Wanneer de techniek goed is, wordt het uithoudingsvermogen een groter aandachtspunt. Daarnaast blijven de zwemmers ook in de doelgroep minioren aan hun techniek werken.
                    De laatste doelgroep binnen de zwemafdeling zijn de junioren en ouder, zij kunnen drie keer in de week trainen. Ook hierbij blijft de techniek belangrijk, maar wordt ook veel op conditie en kracht getraind.
                </p>
                <div class="row swimming-attributes">
                    <div class="mt-2 mb-2 col-4 ">
                        <img class="attributes" src="{{ asset('storage/img/zwemplankje.png') }}" alt="zwemplankje">
                        <p class="font-weight-bold text-center">Zwemplankje</p>
                    </div>
                    <div class="col-4">
                         <img class="attributes" src="{{ asset('storage/img/pullbouy.png') }}" alt="pullbouy">
                        <p class="font-weight-bold text-center">Pullbouy</p>
                    </div>
                    <div class="col-4">
                        <img class="attributes" src="{{ asset('storage/img/zoomers.png') }}" alt="zoomers">
                        <p class="name-attribute font-weight-bold text-center">Zoomers</p>
                    </div>
                </div>
                <p>Na de trainingen worden landtrainingen georganiseerd. Dit zijn trainingen die op de kant of soms buiten plaats vinden. Tijdens deze trainingen worden de spieren getraind door middel van verschillende oefeningen. </p>
            </div>
            <div class="col-6">
                <img class="side-img" src="{{ asset('storage/img/zwemmen1.jpg') }}">
            </div>
        </div>
        <div class="wedstrijden">
            <h3 class="wp-color">Wedstrijden</h3>
            <p>
               Voor de meeste zwemmers is het doel van het trainen om steeds snellere tijden te zwemmen tijdens de wedstrijden.
               De meeste wedstrijden die worden gezwommen zijn in teamverband. Dit betekent dat de hele zwemafdeling die deel mag nemen aan wedstrijden mee gaat. Tijdens de wedstrijd zwem je twee afstanden, en vaak nog een estafette met drie teamgenoten.
               Om een wedstrijd te kunnen zwemmen zijn er verschillende mensen nodig. Allereerst gaat er vanuit de vereniging een coach mee. Hij geeft de laatste aandachtspunten mee voordat de afstand gezwommen wordt. Daarnaast geeft hij tips nadat de afstand gezwommen is.
               Er is een scheidsrechter die zorgt dat de wedstrijd goed verloopt. De starter start de zwemmers weg. De kamprechter kijkt of de zwemmers de slag op de goede manier zwemmen en kijkt in welke volgorde gefinisht word. En de tijdwaarnemers zorgen dat de gezwommen tijden geklokt en genoteerd worden.

               Naast een gemotiveerde inzet van de zwemmers, is gezelligheid binnen de vereniging ook enorm belangrijk. Kom een keer meedoen, en kijk of het je bevalt!
            </p>
        </div>
    </div>
    <div class="c2a" id="foto1">
        <div class="c2a-inner">
            <h3 class=" font-weight-bold text-center">Kom 4 keer gratis meetrainen,
                meld je nu aan! </h3>
            <div class="c2a-btn">
                <a class=" a-btn placing mt-4" data-toggle="modal" data-target="#GratisTrainenModal">
                    <button class="second-btn">aanmelden</button>
                </a>
            </div>
        </div>
    </div>

    @endsection
