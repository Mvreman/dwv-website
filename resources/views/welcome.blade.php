@extends('layouts.app')

@section('content')
    <section id="banner" class="d-flex">
        <div class="banner-inner">
            <div class="actions">
                <img src="{{ asset('storage/img/logo vrijstaand.png') }}">
                <h1>Doesburgse Watersport Vereniging</h1>
                <a href="#1"><img class="arrow" src="{{ asset('storage/img/arrow_down.png') }}"></a>
            </div>
        </div>
    </section>
    <div class="container margins-con" id="1">
        <h2 class="text-center" >Onze mogelijkheden</h2>
        <div class="row my-4">
            <div class="col-4">
                <div class="card select-card">
                    <div class="card-body select-card-body">
                        <img class="card-img" src="{{ asset('storage/img/11.png') }}">
                        <h4 class="text-center select-title">Wedstrijdzwemmen</h4>
                        <p class="text-center">Altijd al meer willen weten over
                            wedstrijdzwemmen?</p>
                        <a class=""><button class="prim-btn select-btn  " >Klik hier</button></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card select-card">
                    <div class="card-body select-card-body">
                        <img class="card-img" src="{{ asset('storage/img/12.png') }}">
                        <h4 class="text-center select-title">Waterpolo</h4>
                        <p class="text-center">Altijd al meer willen weten over
                            waterpolo?</p>
                        <a class=""><button class="prim-btn select-btn" >Klik hier</button></a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card select-card">
                    <div class="card-body select-card-body">
                        <img class="card-img" src="{{ asset('storage/img/Recreanten.png') }}">
                        <h4 class="text-center select-title">Masterzwemmen</h4>
                        <p class="text-center">Altijd al meer willen weten over
                            masterzwemmen?</p>
                        <a class=""><button class="prim-btn select-btn" >Klik hier</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-switch" >
        <div class="container margins-con">
            <div class="row">
                <div class="col-8">
                    <div class="card select-card">
                        <img src="{{ asset('storage/img/2020.png') }}">
                        <div class="card-body">
                        <h3>Names DWV de beste wensen voor 2020</h3>
                        <p>Tijdens de drie gehouden thuiswedstrijden van de waterpolocompetitie 2017-2018, 24 februari jl. in zwembad Den Helder, heeft DWV (Doesburgse Watersport Vereniging) de naam bekend gemaakt van haar nieuwe hoofdsponsor. Retail Bouw Nederland zal de komende drie jaar onlosmakelijk verbonden zijn aan de zwem- en waterpolovereniging uit Doesburg als hoofdsponsor.</p>
                        <a href="#">Lees meer ></a>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card select-card">
                        <div class="modal-header">
                            <h3>Kalender</h3>
                        </div>
                        <div class="card-body">
                            <a href="#">Lees meer ></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container margins-con">
        <img src="{{ asset('storage/img/90.gif') }}" style="width: 100%">
    </div>
    <div class="c2a" id="foto1">
        <div class="c2a-inner">
            <h3 class=" font-weight-bold text-center c2a-text">Kom 4 keer gratis meetrainen, meld je nu aan! </h3>
            <a class=" a-btn placing" data-toggle="modal" data-target="#GratisTrainenModal">
                <button class="second-btn c2a-btn">aanmelden</button>
            </a>
        </div>
    </div>
    <div class="container margins-con">
        <div class="row">
            <div class="col-8 col-pad">
                <h3>Kom een keer meetrainen </h3>
                <p>The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced
                    by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs
                    grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack.
                    Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my
                    junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick,
                    Baz.</p>
                <div class="mt-4    ">
                <a class="a-btn placing mt-4" data-toggle="modal" data-target="#GratisTrainenModal">
                    <button class="prim-btn">Kom een keer meetrainen</button>
                </a>
                </div>
            </div>
            <div class="col-4">
                <iframe class="pro-vid" src="https://www.youtube.com/embed/OQW0jK_h42Y" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <div id="basis-carousel">
            <h2>Sponsors</h2>
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/Dutch-Drying-systems.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/logo1.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/De-Waag-logo.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/kleine.png') }}"></a>
                    </div>
                    <div class="carousel-item">
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/Dutch-Drying-systems.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/logo1.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/De-Waag-logo.png') }}"></a>
                        <a href=""><img class="img-slider carousel-img" src="{{ asset('storage/img/kleine.png') }}"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
